<?php

namespace App\Libs;

use DB;

class PageUtility
{
    /**
     * 検索
     *
     * @param $keyword
     * @return array
     */
    public static function findUserViewedPage($keyword)
    {
        $userPageMap = [];
        $userPageArray = [];

        $pages = DB::table('page')->where('title', 'LIKE', "$keyword%")->orderBy('page.id')->get();
        $activities = DB::table('activity')
                        ->select(DB::raw('`activity`.`page_id`, `activity`.`user_id`, `user`.`name` as user_name, count(1) as view_count'))
                        ->join('user', 'activity.user_id', '=', 'user.id')
                        ->whereIn('activity.page_id', function ($query) use ($keyword) {
                            $query->select('id')
                                ->from('page')
                                ->where('title', 'LIKE', "$keyword%");
                        })
                        ->groupBy('activity.page_id', 'activity.user_id', 'user.name')
                        ->get();
        $activityMap = [];
        foreach ($activities as $activity) {
            $activityMap[$activity->page_id][] = $activity;
        }

        foreach ($pages as $page) {
            if (array_key_exists($page->id, $activityMap)) {
                foreach ($activityMap[$page->id] as $activity) {
                    $userPageMap[$activity->user_id] = [
                        'page_id' => $page->id,
                        'page_title' => $page->title,
                        'user_id' => $activity->user_id,
                        'user_name' => $activity->user_name,
                        'view_count' => $activity->view_count
                    ];
                }
            } else {
                $userPageArray[] = [
                    'page_id' => $page->id,
                    'page_title' => $page->title
                ];
            }
        }

        // ユーザIDでソート
        ksort($userPageMap);

        foreach ($userPageMap as $userPage) {
            $userPageArray[] = $userPage;
        }

        return $userPageArray;
    }
}