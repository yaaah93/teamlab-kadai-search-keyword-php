# チームラボ選考課題

## 修正内容

- [page].[title]にインデックスを追加
- [activity].[page_id]と[user_id]にインデックスを追加
- MySQLのデータバッファサイズを追加
- Eloquentが大量データを読み込む際に遅くなるのでQueryBuilderに変更
- ユーザごとの閲覧数やユーザ名は[activity]に関連付けて取得
- Laravelのconfigをキャッシュ化